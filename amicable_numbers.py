def sum_amicable_numbers(max):
    numbers_done = []
    amicable_numbers = []
    num1 = 1
    while num1< max:
        if num1 not in numbers_done:
            numbers_done.append(num1)
            num1_divisors = []
            for j in range(1,num1):
                if num1 % j == 0:
                    num1_divisors.append(j)
            sum_proper_num1 = sum(num1_divisors)
            num2 = sum_proper_num1
            numbers_done.append(num2)
            num2_divisors = []
            for j in range(1,num2):
                if num2 % j == 0:
                    num2_divisors.append(j)
            sum_proper_num2 = sum(num2_divisors)
            if (sum_proper_num2 == num1) and (num1 != num2) and (sum_proper_num1 == num2):
                amicable_numbers.append(num1)
                print(num1)
                amicable_numbers.append(num2)
                print(num2)
        num1+= 1
    return sum(amicable_numbers)

print(sum_amicable_numbers(10000))